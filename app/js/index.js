function addComment() {
    var value_comment = $("textarea").val();
    $c = $("<div class='comments__all__name'><p><b>Марина Иванова </b> <span class='date'>" + getDateStr() + "</span> </p></div>");
    $(".comments__all").append($c);
    $b = $("<div class='comments__content'>" + value_comment + "</div>");
    $(".comments__all").append($b);
    $(".comments__content").append("<div class='triangle'></div>");
}

function onMessageCtrEnter(event) {
  
    if (event.keyCode == 10 ) {
        addComment();
    }
}

function GetMonth(intMonth) {
    var MonthArray = new Array("января", "февраля", "марта",
        "апреля", "мая", "июня",
        "июля", "августа", "сентября",
        "октября", "ноября", "декабря")
    return MonthArray[intMonth]
}

function getDateStr() {
    var today = new Date();
    var year = today.getYear();
    if (year < 1000) year += 1900;
    var todayStr = today.getDate() + " " + GetMonth(today.getMonth());
    todayStr += ", " + year;
    return todayStr;
}

