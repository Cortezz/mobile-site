'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
gulp.task('sass', function () {
 gulp.src('./sass/**/*.scss')
   .pipe(sass().on('error', sass.logError))
   .pipe(gulp.dest('app/css'))
   .pipe(browserSync.stream());
});


gulp.task('watch', function () {
  browserSync.init({
		server: {
			baseDir : './app'
		},
		notify: false
	})
 gulp.watch('./sass/**/*.scss', ['sass']);
 gulp.watch('app/**/*.html', browserSync.reload);
 gulp.watch('app/**/*.сss', browserSync.reload);
 gulp.watch('app/js/**/*.js', browserSync.reload);
});

gulp.task('default', ['watch']);